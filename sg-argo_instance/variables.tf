# Instance specific variables
variable "aws_ami" {
    default = "ami-fce3c696"
}
variable "owner" {
    default = "ictops"
}
variable "conftag" {
    default = "PROD"
}

# User / Account specific variables
variable "aws_key" { # prompted if 'varInitScript.sh' is not run
    type = "string"
}
variable "aws_named_profile" { # prompted if 'varInitScript.sh' is not run
    type = "string"
}
variable "aws_account" { # prompted if 'varInitScript.sh' is not run
    type = "string"
}
variable "region" {
    default = "us-east-1"
}
variable "aws_availability_zones" {
    type = "map"
    default = {
      default 			=  "us-east-1b,us-east-1d,us-east-1e" # mpto
      aws-digital-sandbox 	=  "us-east-1a,us-east-1b"
      aws-digital-prod		=  "us-east-1a,us-east-1c,us-east-1d,us-east-1e"
      aws-ent-prod 		=  "us-east-1a,us-east-1b,us-east-1d,us-east-1e"
      aws-mpto 			=  "us-east-1b,us-east-1d,us-east-1e"
      aws-news-prod 		=  "us-east-1b,us-east-1c,us-east-1d,us-east-1e"
      aws-sports-prod 		=  "us-east-1a,us-east-1b,us-east-1c,us-east-1e"
    }
}
variable "aws_zone_identifiers" {
    type = "map"
    default = {
      default 			=  "subnet-c5a3cf9d,subnet-7b0f9541,subnet-73dcbb59"			# mpto
      aws-digital-sandbox 	=  "subnet-12f50a64,subnet-019d8b58"
      aws-digital-prod		=  "subnet-00c4285b,subnet-36eb051b,subnet-8d38e6c4,subnet-503cde6c"
      aws-ent-prod 		=  "subnet-9a99c1ec,subnet-e81661b0,subnet-a3066f89,subnet-51bde66c"
      aws-mpto 			=  "subnet-c5a3cf9d,subnet-7b0f9541,subnet-73dcbb59"
      aws-news-prod 		=  "subnet-d9102bf3,subnet-8cc229c5,subnet-b5d0ebed,subnet-d4343ce9"
      aws-sports-prod 		=  "subnet-6609684c,subnet-e083d396,subnet-a083ecf8,subnet-37cf930a"
    }
}
variable "aws_vpc" {
    type = "map"
    default = {
      default 			=  "vpc-f4f05d91" # mpto
      aws-digital-sandbox 	=  "vpc-d069efb4"
      aws-digital-prod		=  "vpc-5babf63c"
      aws-ent-prod 		=  "vpc-0b94506c"
      aws-mpto 			=  "vpc-f4f05d91"
      aws-news-prod 		=  "vpc-76aacd11"
      aws-sports-prod 		=  "vpc-c441b3a3"
    }
}


