# Application specific variables
variable "environment" {
    default = "<ENVIRONMENT>"
}
variable "product" {
    default = "<PRODUCT_NAME>"
}
variable "project" {
    default = "<PROJECT>"
}
variable "customer" {
    default = "<CUSTOMER>"
}
variable "team" {
    default = "ictops"
}
variable "min_instances" {
    default = 0
}
variable "max_instances" {
    default = 2
}
variable "desired_instances" {
    default = 1
}

# Instance specific variables
variable "package_size" {
    default = "2x4x32-c3"
}
variable "aws_instance_type" {
    default = "c3.large"
}
