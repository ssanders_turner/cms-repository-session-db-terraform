# Create/Update/Destroy the AWS resources dictated by variables.tf

# Configure the AWS Provider
provider "aws" {
  profile = "${var.aws_named_profile}"
  region = "${var.region}"
}

# Provide user_data for instances or asg's
module "bootstrap" {
  source       = "git::ssh://git@bitbucket.org/vgtf/argo-bootstrap-terraform.git?ref=v2.0.0"
  products     = "${var.product}:${var.environment}"
  package_size = "${var.package_size}"
  customer     = "${var.customer}"
  conftag      = "${var.conftag}"
  owner        = "${var.owner}"
  environment  = "${var.environment}"
  chassis      = "EC2_VIRTUAL"
  location     = "ec2"
  network      = "prod"
}

# Create Security Group (SG) - referenced by LC below - suffixed for safety
resource "aws_security_group" "sg-tf" {
  name = "${var.product}-${var.environment}-example"
  vpc_id = "${lookup(var.aws_vpc, var.aws_account)}"
  description = "Allow all inbound for ${var.product}-${var.environment}"

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "aws-example-instance" {
  user_data = "${module.bootstrap.user_data}"
  ami = "${var.aws_ami}"
  instance_initiated_shutdown_behavior = "stop"
  instance_type = "${var.aws_instance_type}"
#  monitoring = true
  disable_api_termination = false
  availability_zone = "${element(split(",", lookup(var.aws_availability_zones, var.aws_account)), 0)}"
  subnet_id = "${element(split(",", lookup(var.aws_zone_identifiers, var.aws_account)), 0)}"
  vpc_security_group_ids = ["${aws_security_group.sg-tf.id}"]

  tags {
     "Name" = "${var.product}_${var.environment}"
     "creator" = "terraform"
     "product" = "${var.project}"
     "customer" = "${var.customer}"
     "team" = "${var.team}"
     "environment" = "${var.environment}"
     "billing" = "${var.customer}"
     "application" = "${var.project}"
  }
}
