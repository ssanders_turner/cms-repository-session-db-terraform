#!/bin/bash

# script exports your AWS Key Name, AWS Named Profile, and AWS Account Name
# if you choose to not run (source) this script you will be prompted for these 
# variables when running terraform

#TF_VAR_aws_account="aws-digital-prod"
TF_VAR_aws_account="aws-digital-sandbox"
TF_VAR_aws_key="chris.jones@turner.com"
#TF_VAR_aws_named_profile="digitalprod"
TF_VAR_aws_named_profile="digitalsandbox"

echo "Setting TF_VAR's to:"
echo $TF_VAR_aws_account $TF_VAR_aws_key $TF_VAR_aws_named_profile
export TF_VAR_aws_account TF_VAR_aws_key TF_VAR_aws_named_profile
