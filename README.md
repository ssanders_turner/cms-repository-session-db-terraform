# Example Terraform scripts

## Using template-terraform
  template-terraform directory in this repo can be copied and modified per
  the "TEMPLATE_README.md" file.

## Checkout mss-terraform-utilities
 https://bitbucket.org/vgtf/mss-terraform-utilities/overview

## varInitScript.sh
  Modify and run varInitScript.sh per your account/aws details.  If script is
  not run then all terraform commands will prompt you for those details.

```sh
source varInitScript.sh
```

  Alternatively you can export them via any other method prefered following 
  this syntax:

```sh
export TF_VAR_aws_account=<aws_account_name>
export TF_VAR_aws_key="<aws_key_pair_name>"
export TF_VAR_aws_named_profile=<aws_named_profile>
```

## Terraform commands
```sh
cd $EXAMPLE/$ENVIRONMENT
```
  
```sh
# make appropriate changes to infrastructure

terraform get -update=true
terraform plan
```
  
```sh
# make sure all changes are correct

terraform apply
```

```sh
# destroy all resources

terraform destroy
```

## ACCESS
  http://docs.turner.com/display/IOAppOps/Argo+Terraform

  If the person running the scripts does not have access to the correct account
  these scripts will fail. If the person does have access, then make sure there
  is a profile associtated with the account in the ~/.aws/credentials file.
