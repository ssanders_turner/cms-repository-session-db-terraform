# Create/Update/Destroy the AWS resources dictated by variables.tf

# Configure the AWS Provider
provider "aws" {
  profile = "${var.aws_named_profile}"
  region = "${var.region}"
}

# Provide user_data for instances or asg's
module "bootstrap" {
  source       = "git::ssh://git@bitbucket.org/vgtf/argo-bootstrap-terraform.git?ref=v2.0.0"
  products     = "${var.product}:${var.environment}"
  package_size = "${var.package_size}"
  customer     = "${var.customer}"
  conftag      = "${var.conftag}"
  owner        = "${var.owner}"
  environment  = "${var.environment}"
  chassis      = "EC2_VIRTUAL"
  location     = "ec2"
  network      = "prod"
}

# Create Security Group (SG) - referenced by LC below - suffixed for safety
resource "aws_security_group" "sg-tf" {
  name = "${var.product}-${var.environment}-example"
  vpc_id = "${lookup(var.aws_vpc, var.aws_account)}"
  description = "Allow all inbound for ${var.product}-${var.environment}"

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create Launch Configuration (LC) choosing AMI, Type, and pass Bootstrap data (user_data)
resource "aws_launch_configuration" "lc-tf" {
  name = "${var.product}-${var.environment}-example"
  image_id = "${var.aws_ami}"
  instance_type = "${var.aws_instance_type}"
  security_groups = ["${aws_security_group.sg-tf.id}"]
  user_data = "${module.bootstrap.user_data}"
}

# Create Auto Scaling Group (ASG) using LC above; Note the 3 required Turner Cloud tags
resource "aws_autoscaling_group" "asg-tf" {
  name = "${var.product}-${var.environment}-example"
  depends_on = ["aws_launch_configuration.lc-tf"]
  launch_configuration = "${var.product}-${var.environment}-example"
  min_size = "${var.min_instances}"
  max_size = "${var.max_instances}"
  desired_capacity = "${var.desired_instances}"
  availability_zones = ["${split( ",", lookup(var.aws_availability_zones, var.aws_account))}"]
  vpc_zone_identifier = ["${lookup(var.aws_zone_identifiers, var.aws_account)}"]
  tag {
    key = "Name"
    value = "${var.product}-${var.environment}-asg"
    propagate_at_launch = "true"
  }
  tag {
    key = "environment"
    value = "${var.environment}"
    propagate_at_launch = "true"
  }
  tag {
    key = "application"
    value = "${var.product}"
    propagate_at_launch = "true"
  }
  tag {
    key = "team"
    value = "${var.customer}"
    propagate_at_launch = "true"
  }

}

