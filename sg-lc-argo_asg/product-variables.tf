# Application specific variables
variable "environment" {
    default = "dev"
}
variable "product" {
    default = "mss-sslcheck-app"
}
variable "project" {
    default = "ssl"
}
variable "customer" {
    default = "mss"
}
variable "team" {
    default = "ictops"
}
variable "min_instances" {
    default = 0
}
variable "max_instances" {
    default = 4
}
variable "desired_instances" {
    default = 1
}

# Instance specific variables
variable "package_size" {
    #default = "2x4x32-c3"
    default = "1x1x8-t2"
}
variable "aws_instance_type" {
    #default = "c3.large"
    default = "t2.micro"
}
