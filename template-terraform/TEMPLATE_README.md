# Usage

  To use this Terraform template copy the entire folder
  to the name of your new terraform plan and modify the following
  files and replace <PRODUCT_NAME>, <ENVIRONMENT>, <PROJECT>, and 
  <CUSTOMER> (this is the Argo users-group that will be added)
  with values corresponding to your application.

  - README.md 
  - product-variables.tf

  Modify varInitScript.sh to use the correct AWS 
  Account name and AWS Named Profile (to match ~/.aws/config)

