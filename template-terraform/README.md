# <PRODUCT_NAME> Terraform

  Creates the infrastructure needed to run <PRODUCT_NAME>
  site and services - An Argo Deployment

## Requirements
  The mappings in the variables file requires you to be on
  Terraform Version 0.7.0 or higher.

## Structure

 - <ENVIRONMENT>

## Instructions

```sh
vi varInitScript.sh # modify per user/account
source varInitScript.sh # will be prompted for user/account/profile if this isn't run
cd <ENVIRONMENT>
terraform get -update=true
terraform plan
terraform apply
```

## Checkout mss-terraform-utilities
  Checkout utility repo and add location to your PATH

  https://bitbucket.org/vgtf/mss-terraform-utilities/overview

## Get IPs
  Run 'get-aws-ips' to obtain all instance ipaddresses from any 
  deployed auto scale groups or instances
```sh
cd <ENVIRONMENT>
get-aws-ips
```

